README

Project 3: Breakout Clone

Game has added physics to bring momentum to the ball from the paddle. Paddle speed is also based on distance to the ball to try and catch up which brings a different momentum to the ball. Made the ball, and actual ball, supposed to be a sun, but the fire doesn't trail very well. For the extra style I added some funky colors... I hope we appreciate the funky colors.

Name: Matthew Williamson

Email: mwilli20@binghamton.edu

B-Number: B00335701
