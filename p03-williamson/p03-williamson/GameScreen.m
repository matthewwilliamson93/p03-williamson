//
//  GameScreen.m
//  p03-williamson
//
//  Created by Matthew Williamson on 2/9/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import "GameScreen.h"

@implementation GameScreen
@synthesize paddle, ball;
@synthesize timer;
@synthesize score;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

NSMutableArray *bricks;
int fixedY = 450;
bool playing = false;
int scoreV = 0;
int mom = 0;

// Source: https://gist.github.com/kylefox/1689973
-(UIColor*)randColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

-(void)createPlayField {
    paddle = [[UIView alloc] initWithFrame:CGRectMake(115, fixedY, 60, 10)];
    [self addSubview:paddle];
    [paddle setBackgroundColor:[UIColor blackColor]];
    
    ball = [[UIView alloc] initWithFrame: CGRectMake(140, fixedY - 20, 12, 12)];
    ball.layer.cornerRadius = 6;
    [self addSubview:ball];
    [ball setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sun.png"]]];
    
    bricks = [[NSMutableArray alloc] init];
    for (int j = 0; j < 3; j++) {
        for (int i = 0; i < 7; i++) {
            [bricks addObject:[[UIView alloc] initWithFrame:CGRectMake(40 * i + 5, 30 * j + 60, 35, 15)]];
            [self addSubview:[bricks lastObject]];
            [[bricks lastObject] setBackgroundColor:[self randColor]];
        }
    }
    
    dx = 5;
    dy = -5;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches) {
        if (!playing) {
            return;
        }
        CGPoint p = [t locationInView:self];
        p.y = fixedY;
        
        if ([paddle center].x - p.x > 20) {
            p.x = [paddle center].x - 9;
            mom = -3;
        }
        else if ([paddle center].x - p.x > 10) {
            p.x = [paddle center].x - 4;
            mom = -1;
        }
        else if ([paddle center].x - p.x < 20) {
            p.x = [paddle center].x + 9;
            mom = 3;
        }
        else if ([paddle center].x - p.x < 10) {
            p.x = [paddle center].x + 4;
            mom = 1;
        }
        else {
            mom = 0;
        }
        
        int buf = 32;
        if (p.x <= buf) {
            p.x = buf;
        }
        else if (p.x >= [self bounds].size.width - buf) {
            p.x = [self bounds].size.width - buf;
        }
        [paddle setCenter:p];
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self touchesBegan:touches withEvent:event];
}

-(IBAction)startAnimation:(id)sender {
    timer = [NSTimer scheduledTimerWithTimeInterval:.025 target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    playing = true;
    
}

-(IBAction)stopAnimation:(id)sender {
    [timer invalidate];
    playing = false;
}

-(IBAction)resetAnimation:(id)sender {
    [paddle setFrame:CGRectMake(115, fixedY, 60, 10)];
    [ball setFrame:CGRectMake(140, fixedY - 20, 10, 10)];
    for (int i = 0; i < [bricks count]; i++) {
        [bricks[i] setHidden:NO];
        [bricks[i] setBackgroundColor:[self randColor]];
    }
    dx = 5;
    dy = -5;
    [timer invalidate];
    playing = false;
    scoreV = 0;
    [score setText:@"_______"];
}

-(void)timerEvent:(id)sender {
    CGPoint p = [ball center];
    CGRect bounds = [self bounds];

    if (p.x + dx < 0 || p.x + dx > bounds.size.width) {
        dx = -dx;
    }
    else if (p.y + dy < 0) {
        dy = -dy;
    }
    else if (p.y + dy > bounds.size.height - 20) {
        [timer invalidate];
        playing = false;
        [score setText:@"Game Over!"];
    }
    
    p.x += dx;
    p.y += dy;
    [ball setCenter:p];

    if (CGRectIntersectsRect([ball frame], [paddle frame])) {
        dy = -dy;
        p.y += 2 * dy;
        dx += mom;
        [ball setCenter:p];
    }
    
    for (int i = 0; i < [bricks count]; i++) {
        if (CGRectIntersectsRect([ball frame], [bricks[i] frame]) && [bricks[i] isHidden] == NO) {
            bounds = [bricks[i] bounds];
            if (fabs(bounds.origin.y - p.y) < 1 || fabs(bounds.size.width - p.y) < 1) {
                dx = -dx;
                p.x += 2 * dx;
            }
            else {
                dy = -dy;
                p.y += 2 * dy;
            }
            
            [ball setCenter:p];
            [bricks[i] setHidden:YES];
            scoreV += 10;
            [score setText:[@(scoreV) stringValue]];
            if (scoreV == 210) {
                [timer invalidate];
                playing = false;
                [score setText:@"You Win!"];
            }
        }
    }
}

@end