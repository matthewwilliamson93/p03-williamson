//
//  ViewController.h
//  p03-williamson
//
//  Created by Matthew Williamson on 2/9/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameScreen.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameScreen *gameScreen;

@end

